# Distributed Systems Course @NTUA
[Course](http://www.cslab.ece.ntua.gr/courses/distrib/)

## Team Members
+ Michalis Papadopoullos (@xmpf)
+ Charalambos Kardaris (@ckardaris)
+ Ioannis Asmanis (@\_JackFrost\_)

## Resources for exams:

### Video Lectures
[Playlist: Illinois Cloud Computing Concepts](https://www.youtube.com/watch?v=Vw7UxHlyDyA&list=PLFd87qVsaLhOkTLvfp6MC94iFa_1c9wrU)  
[Playlist - Distributed Systems Course](https://www.youtube.com/playlist?list=PLOE1GTZ5ouRPbpTnrZ3Wqjamfwn_Q5Y9A)  

### Texts
[A simple Distributed Hash Table (DHT)](https://rezahok.wordpress.com/2009/09/21/a-simple-distributed-hash-table-dht/)  
[CS 264. Peer-to-Peer Systems](http://www.eecs.harvard.edu/~mema/courses/cs264/cs264.html)  
[Distributed Hash Tables - DHT](https://cse.buffalo.edu/~eblanton/course/cse586-2018-0s/materials/2018S/15-dht.pdf)  
[Distributed Hash Tables - DHT (Princeton)](https://www.cs.princeton.edu/courses/archive/spr11/cos461/docs/lec22-dhts.pdf)  
[MapReduce Examples](https://meri-stuff.blogspot.com/2011/10/mapreduce-questions-and-answers.html)  

### Podcasts
[Kademlia: P2P Distributed Hash Table with Petar Maymounkov](https://softwareengineeringdaily.com/2018/07/02/kademlia-p2p-distributed-hash-table-with-petar-maymounkov/)  

## Resources for project:
[A Practical Introduction to Blockchain with Python](http://adilmoujahid.com/posts/2018/03/intro-blockchain-bitcoin-python/) [Github Repo](https://github.com/adilmoujahid/blockchain-python-tutorial)  
[Blockchain Demo](https://anders.com/blockchain/blockchain.html)  
[Dumbcoin - An educational python implementation of a bitcoin-like blockchain](https://github.com/julienr/ipynb_playground/blob/master/bitcoin/dumbcoin/dumbcoin.ipynb)  