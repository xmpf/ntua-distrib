#!/usr/bin/env python3

import binascii
import time
from Crypto.Hash import SHA256
from Crypto.PublicKey import RSA
from Crypto.Signature import PKCS1_v1_5 as PKCS

from money_class import Money

from debug_utils import diagnostic

def key2String(someKey, delim=":"):
	if not someKey:	# genesis handling
		return ''
	return (str(someKey.publickey().n)+delim+str(someKey.publickey().e))

def string2Key(someString, delim=":"):
	if not someString:
		raise Exception("Unable to convert String to Key")
	publicKeyExps = someString.split(delim)
	return RSA.construct(
		(
			int(publicKeyExps[0]), # component: n
			int(publicKeyExps[1])  # component: e
		)
	)

class Transaction:
	def __init__(self, inputMoney, toAddress, fromAddress, amount, outputMoney=None):
		diagnostic ("Transaction::init")
		self.toAddress = toAddress
		self.fromAddress = fromAddress
		self.amount = amount
		self.inputs = inputMoney
		self.uid = SHA256.new(self.toRawStr().encode("utf8")).hexdigest()

		utxoSum = 0
		if ((not isinstance(fromAddress, int)) and inputMoney):
			for inp in self.inputs:
				utxoSum += inp.amount
		# assert(utxoSum >= self.amount)	# make sure total incoming is at least equal to amount
		if (outputMoney is None):	# if nothing is given for output, compute it
			if (utxoSum-self.amount != 0):
				# only one output if the UTXO matches exactly, no zero-money instances
				self.outputs = [Money(self.uid, utxoSum-self.amount, self.fromAddress)]
			else:
				self.outputs = []
			self.outputs.append(Money(self.uid, self.amount, self.toAddress))
		else:	# else check that output is list of 2 things and money adds up
			self.outputs = outputMoney
		self.signature = None

	def __hash__(self):
		diagnostic ("Transaction::hash")
		return int(self.uid, 16)

	# signTransaction :: Transaction -> PrivateKey -> Signature
	def signTransaction(self, privateKey):
		diagnostic ("Transaction::signTransaction")
		#signingKey = RSA.importKey(binascii.unhexlify(privateKey))
		signer = PKCS.new(privateKey)
		data = str(self).encode("utf8")
		digest = SHA256.new(data)
		self.signature = signer.sign(digest)
		# Must work or we have failed at signing level
		assert(self.verifyTransaction())

	# verifyTransaction :: Transaction -> PublicKey -> Boolean
	def verifyTransaction(self) -> bool:
		"""
		verifyTransaction: Returns True if signature matches
		"""
		diagnostic ("Transaction::verifyTransaction")
		if (isinstance(self.fromAddress, int)):	# Genesis block has int as key, normally illegal
			return True

		if (not self.fromAddress):
			return True

		if (not self.signature):
			print("ERROR: Transaction is not signed at all!")
			return False

		publicKey = self.fromAddress #RSA.importKey(binascii.unhexlify(senderAddress)) # (bug) !!
		verifier = PKCS.new(publicKey)
		data = str(self).encode("utf8")
		digest = SHA256.new(data)
		givenSig = self.signature
		return verifier.verify(digest, givenSig)

	# getChange :: Transaction -> PublicKey -> Double
	# def getChange(self, accountKey) -> float:
	# 	"""
	# 	Get balance change for an account due to this tx.
	# 	If the account took part in the transaction as the toAccount,
	# 	the amount gets added, if it was the fromAccount, it gets
	# 	subtracted. Else zero change.
	# 	"""
	# 	print ("Transaction::getChange")
	# 	ret = 0.0
	# 	if (self.fromAddress.publickey() == accountKey.publickey()):
	# 		ret -= self.amount
	# 	if (self.toAddress.publickey() == accountKey.publickey()):
	# 		ret += self.amount
	#
	# 	print(
	# 		self.serialize(), accountKey, ret
	# 	)
	# 	return ret

	def stringifyMoneyList(self, moneyList):
		diagnostic ("Transaction::stringifyMoneyList")
		if (not moneyList):
			return ''
		else:
			return str([str(inp) for inp in self.inputs])

	def toRawStr(self) -> str:
		"""
		return "To: " + str(self.toAddress) + \
			", From: " + str(self.fromAddress) + \
			", Amount: " + str(self.amount) + \
			", Timestamp: " + str(self.tid) + \
			", Signature: " + str(self.signature)
		"""
		diagnostic ("Transaction::toRawStr")
		return  key2String(self.toAddress) + \
				key2String(self.fromAddress) + \
				self.stringifyMoneyList(self.inputs) + \
				str(self.amount)

	# str :: Transaction -> String
	def __str__(self) -> str:
		"""
		return "To: " + str(self.toAddress) + \
			", From: " + str(self.fromAddress) + \
			", Amount: " + str(self.amount) + \
			", Timestamp: " + str(self.tid) + \
			", Signature: " + str(self.signature)
		"""
		diagnostic ("Transaction::str")
		return  key2String(self.toAddress) + \
				key2String(self.fromAddress) + \
				self.stringifyMoneyList(self.inputs) + \
				str(self.amount) + \
				str(self.uid)

	def __eq__(self, other) -> bool:
		diagnostic ("Transaction::eq")
		if isinstance(other, Transaction):
			return self.uid == other.uid \
				and self.fromAddress.publickey() == other.fromAddress.publickey() \
				and self.toAddress.publickey() == other.toAddress.publickey() \
				and self.inputs == other.inputs \
				and self.amount == other.amount
		else:
			raise Exception("Expected Instance Transaction")

	def serialize(self):
		diagnostic ("Transaction::serialize")
		inputs = []
		if (self.inputs):
			if (len(self.inputs) > 0):
				inputs = [inp.serialize() for inp in self.inputs]

		outputs = []
		outputs = [outp.serialize() for outp in self.outputs]

		# TODO:
		# THERE IS A BUG FROM GENESIS BLOCK
		# WHERE \self.fromAddress\ == 1
		if (not self.fromAddress):
			from_ = 1
		else:
			from_ = key2String(self.fromAddress)
		return {
			'trans_id': self.uid,
			'from': from_,
		    'to': key2String(self.toAddress),
		    'amount': self.amount,
		    'inputs': inputs,
			'outputs': outputs,
			'signature': (binascii.hexlify(self.signature).decode('utf-8') if (self.signature) else None)
		}
