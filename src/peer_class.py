#!/usr/bin/env python3

from transaction_class import key2String

from miner_configuration import NUM_PEERS, START_BALANCE

from debug_utils import diagnostic

class Peer:
    def __init__(self, url: str, id: int, publicKey):
        diagnostic ("Peer::init")
        self.url = url
        self.id = id
        self.publicKey = publicKey
        self.utxos = []

    def getFundsForAmount(self, amount):
        diagnostic ("Peer::getFundsForAmount")
        tempSum = 0
        res = []
        for u in self.utxos:
            tempSum += u.amount
            res.append(u)
            if (tempSum >= amount):
                return res

        print("ABORTED - Insufficient funds for node: {}, amount was: {}"
            .format(self.id, amount))
        return None

    # def bootstrapUtxosInit(self):
    #     print("Peer::bootstrapUtxosInit")
    #     if (self.id > 0):
    #         print("ABORTED - Only bootstrap can initialize funds for themselves")
    #     self.utxos.append(Money(None, NUM_PEERS * START_BALANCE))

    def updateUtxos(self, gone, added):
        diagnostic ("Peer::updateUtxos - ID = " + str(self.id))
        diagnostic(gone)
        diagnostic(self.utxos)
        if (gone):
            if (len(gone) > 0):
                diagnostic("SELF:")
                [diagnostic(str(u)) for u in self.utxos]
                diagnostic("GONE:")
                [diagnostic(str(u)) for u in gone]
                for g in gone:
                    self.utxos.remove(g)
        if (added):
            diagnostic("ADDING")
            if (added.amount > 0):
                diagnostic(str(added))
                self.utxos.append(added)
        diagnostic("FINALLY")
        diagnostic(self.utxos)

    def getTotalFunds(self):
        diagnostic ("Peer::getTotalFunds")
        sum = 0.0
        for u in self.utxos:
            sum += u.amount
        return sum

    def __str__(self):
        diagnostic ("Transaction::str")
        return str(self.url) + ", " + \
            str(self.id) + ", " + \
            str(self.publicKey.publickey().n) + ", " + \
            str(self.publicKey.publickey().e)

    def __eq__(self, other) -> bool:
        diagnostic ("Transaction::eq")
        if isinstance(other, Peer):
            return self.url == other.url \
               or self.publicKey == other.publicKey
        else:
            raise Exception("Not instance of Peer class")
        return False

    def serialize(self):
        diagnostic ("Transaction::serialize")
        return {
            'ipAddr': self.url
          , 'pid': self.id
          , 'publicKey': key2String(self.publicKey)
        }
