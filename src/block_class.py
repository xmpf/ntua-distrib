#!/usr/bin/env python3

import hashlib
import time

# Transaction Class
from transaction_class import Transaction

# Configuration Options
from miner_configuration import BLOCK_CAPACITY

from debug_utils import diagnostic

class Block:
    def __init__(self, data: str, previousHash: str, valid=False, genesisBlock=False):
        diagnostic("Block::init")
        # self.height = height
        self.data = data                    # data
        self.transactions = []              # transactions of block
        self.previousHash = previousHash    # previous hash
        self.nonce = 0                      # nonce
        self.valid = valid                  # True if mined, False if pending
        self.hash = self.computeHash()      # block hash
        self.isGenesis = genesisBlock       # True if genesis, False if simple
        self.txSet = set()
        if (self.isGenesis):
            self.valid = True

    # len :: Block -> Integer
    def __len__(self) -> int:
        """
        Return length of tx list
        """
        diagnostic("Block::len")
        return len(self.transactions)

    def __str__(self) -> str:
        """
        Get a basic string representation for printing
        """
        diagnostic("Block::str")
        return 'Block: ' \
            + str([str(tx) for tx in self.transactions]) + ', ' \
            + str(self.hash) + ', ' \
            + str(self.valid) + ', ' \
            + str(self.previousHash)

    # computeHash :: Block -> String
    def computeHash(self) -> str:
        """
        Usage: If called on an unvalidated block, iterates nonce and
        computes next possible hash. If called on a validated block, simply
        returns the solution hash.
        """
        diagnostic ("Block::computeHash")
        sha256_digest = hashlib.sha256()
        sha256_digest.update ((str(self.data) \
                             + str(self.previousHash) \
                             + str([str(tx) for tx in self.transactions]) \
                             + str(self.nonce)).encode())
        if (not self.valid):
            self.nonce += 1   # while invalid this contains the next nonce to check
        return sha256_digest.hexdigest()

    # validateBlock :: Block -> ()
    def validateBlock(self):
        """
        When nonce has met the external difficulty requirements this gets called
        """
        diagnostic ("Block::validateBlock")
        self.nonce -= 1 # decrement nonce because it overshoots
        self.valid = True
        self.hash = self.computeHash()

    # isValid :: Block -> Boolean
    def isValid(self) -> bool:
        """
        Check block validity
        """
        diagnostic ("Block::isValid")
        return self.valid or self.isGenesis

    # isMinable :: Block -> Boolean
    def isMinable(self) -> bool:
        """
        A block is minable iff it is invalid and the number of transactions has
        reached the specified block capacity
        """
        diagnostic ("Block::isMinable")
        return (not self.isValid()) and (len(self.transactions) == BLOCK_CAPACITY)

    # addTransaction :: Block -> Transaction -> Block
    def addTransaction(self, transaction: Transaction):
        """
        Add transaction to the block
        """
        diagnostic ("Block::addTransaction")
        if (not transaction.fromAddress): # Genesis
            diagnostic('GENESIS TRANSACTION REGISTERED')
            self.transactions.append(transaction)
            self.txSet.add(transaction)
            return True

        if (not transaction.verifyTransaction()):
            print("ERROR: Transaction not verified by sender's signature!") # (bug) !!

        if len(self) > 0:   # if list already contains txs
            for t in self.transactions:
                if t == transaction:
                    # require uniqueness
                    print("Transaction # {:} already exists.".format(transaction.tid))
                    return False
            if (len(self) >= BLOCK_CAPACITY
                or self.isValid()):   # if at or above block capacity fail
                return False
            self.transactions.append(transaction) # else append it
            self.txSet.add(transaction)
        else:
            self.transactions = [transaction]   # else create list and add tx there
            self.txSet.add(transaction)
        return True # success

    def verifyBlockTxs(self):
        """
        Used to verify that all txs in block are properly signed
        """
        diagnostic("Block::verifyBlockTxs")
        if (self.isGenesis):
            return True
        for t in self.transactions:
            if not t.verifyTransaction():
                return False
        return True

    def containsTx(self, tx):
        diagnostic ("Block::containsTx")
        return(tx in self.transactions)

    def semEquiv(self, other):
        diagnostic ("Block::semEquiv")
        return (self.txSet == other.txSet)

    # def getTotalChange(self, accountKey: str) -> float:
    #     """
    #     Total change in account balance due to txs in this block
    #     """
    #     print ("Block::getTotalChange")
    #     ret = 0.0
    #     for t in self.transactions:
    #         ret += t.getChange(accountKey)
    #     return ret

    def serialize(self):
        diagnostic("Block::serialize")
        if len(self.transactions) > 0:
            transactions = [x.serialize() for x in self.transactions]
        else:
            transactions = []
        return {
            'data': self.data,
            'transactions': transactions,
            'previousHash': self.previousHash,
            'hash': self.hash,
            'nonce': self.nonce,
        }
