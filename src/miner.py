#####################################################################################################################
# Title: NoobCoin
# Description: Project for Distributed Systems Course @ NTUA (2018 - 2019)
#
#
# References:
# [1] https://github.com/clamytoe/pychain/blob/master/pychain/blockchain.py
#
#
#####################################################################################################################

#!/usr/bin/env python3

# Import Section
import sys
import time
import hashlib
import json
import requests
import base64
import threading
#import ecdsa

from Crypto.PublicKey import RSA

# Wallet Class
from wallet_class import Wallet

# Block Class
from block_class import Block

#Peer Class
from peer_class import *

# Transaction Class
from transaction_class import *

# Blockchain Class
from blockchain_class import Blockchain

# flask -> jsonify
from flask import jsonify

import builtins

# REST API
#from flask import Flask, request

# Multithreading support
from multiprocessing import Process, Pipe

from utils import *
from debug_utils import diagnostic

# Configuration Options
from miner_configuration import *

# MINER_ADDRESS: GENERATED ADDRESS
# MINER_NODE_URL:
# PEER_NODES: LIST OF CONNECTED NODES

#node = Flask(__name__)

STATE_TOO_SOON = -1
STATE_ID_HANDOUT = 0
STATE_GENESIS = 1
STATE_OK = 2

class Miner:
	def __init__(self, isBootstrap=False):
		diagnostic("Miner::init")
		self.wallet = Wallet()
		self.bootstrap = isBootstrap
		self.state = STATE_TOO_SOON
		self.id = -1
		self.peers = []
		if (isBootstrap):
			self.id = 0
			self.wallet.id = 0
			self.peers = self.initializePeerList()
			genTx = self.wallet.generateInitialBlock()
			self.finalizeTransaction(genTx)

		# if (self.wallet.noobchain == None):
		# 	raise Exception("Blockchain empty")
		# else:
		# 	self.assumed_balance = self.wallet.noobchain.getCurrentBalance(self.wallet.pubAddress)


	def initializePeerList(self):
		diagnostic ("Miner::initializePeerList")
		self.state = STATE_ID_HANDOUT
		return [Peer(BOOTSTRAP_NODE_URL, self.id, self.wallet.publicKey)]

	def getNextId(self, publicKey, ipAddr):
		diagnostic ("Miner::getNextId")
		self.expandPeers({"peers": [{"ipAddr": ipAddr, "publicKey": publicKey, "pid": len(self.peers)}]})
		return self.peers[-1].id

	def expandPeers(self, data):
		diagnostic ("Miner::expandPeers")
		peers = list(data['peers'])
		diagnostic("NEW NODE DATA:")
		diagnostic(data['peers'])
		for p in peers:
			ipAddr = p["ipAddr"]
			publicKey = string2Key(p["publicKey"])
			pid = p["pid"]
			candidate = Peer(ipAddr, int(pid), publicKey)
			if not (candidate in self.peers):
				self.peers.append(candidate)

		diagnostic("AFTER EXPANSION:")
		[diagnostic(p) for p in self.peers]


	def networkCommBootstrap(self):
		diagnostic ("Miner::networkCommBootstrap")
		stuff = {"full_url": MINER_NODE_URL, "publicKey": key2String(self.wallet.publicKey)}
		data = requests.post(BOOTSTRAP_NODE_URL+'/idHandout', json=stuff).json()
		bootstrapPublicKey = string2Key(data["publicKey"])
		self.peers.append(Peer(BOOTSTRAP_NODE_URL, 0, bootstrapPublicKey))
		self.id = int(data["id"])
		self.wallet.id = self.id
		self.peers.append(Peer(MINER_NODE_URL, self.id, self.wallet.publicKey))
		wakeUpCall = requests.get(BOOTSTRAP_NODE_URL+'/broadcastCheck')
		diagnostic("WAKEY-WAKEYYYYY")
		diagnostic(wakeUpCall)

	def viewLastBlock(self):
		"""
		Frontend spec, display last validated block.
		"""
		diagnostic ("Miner::viewLastBlock")
		if (self.wallet.noobchain is not None):
			return str(self.wallet.noobchain.getLastValidatedBlock())

	def consensus(self, foreign_bc: Blockchain):
		"""
		Apply simple consensus algorithm
		"""
		diagnostic ("Miner::consensus")
		if (self.betterChain(foreign_bc)):
			self.wallet.noobchain = foreign_bc
			self.runAllTxs()
		else:
			diagnostic("INCOMING BLOCKCHAIN REJECTED")

	def runAllTxs(self, bc=None):
		"""
		Elementary function to execute EVERY transaction in the current blockchain
		"""
		diagnostic("Miner::runAllTxs")
		if (not bc):
			bc = self.wallet.noobchain
		diagnostic('>>> REBOOTING BLOCKCHAIN <<<')
		for p in self.peers:
			p.utxos = []
		for block in bc.blockchain:
			for tx in block.transactions:
				self.finalizeTransaction(tx)

	def printGlobalWalletState(self):
		"""
		Frontend spec - all balances
		"""
		for p in self.peers:
			self.printWalletBalance(account=p.publicKey)

	def printWalletBalance(self, account=None):
		"""
		Frontend spec - present balance in NBCs
		"""
		diagnostic ("Miner::getWalletBalance")
		if (not account):
			account = self.wallet.publicKey
		p = self.getPeerByKey(account)
		print('ID: {:}\tFunds: {:}'.format(
				p.id,
				p.getTotalFunds()
			)
		)

	def betterChain(self, fbc: Blockchain) -> bool:
		"""
		Check if a foreign blockchain should be adopted, that is:
			1. contains only verified (signed) transactions in its blocks
			2. has blocks with a proper hashing sequence
			3. has a greater length than ours
			4. if it is the bootstrap chain
		"""
		diagnostic ("Miner::betterChain")
		return (fbc.verifyBlockchain()
			and fbc.checkValidity()
			and (len(fbc) > len(self.wallet.noobchain) or len(self.wallet.noobchain) == 0))

	def incomingTransaction(self, tx):
		"""
		Add a new transaction (which was externally given) to the blockchain.
		"""
		diagnostic ("Miner::incomingTransaction")
		if (tx.verifyTransaction()):
			self.wallet.noobchain.addBlock(tx)
			self.finalizeTransaction(tx)
		else:
			diagnostic("INCOMING TRANSACTION REJECTED")

	def incomingBlock(self, block, senderID):
		diagnostic ("Miner::incomingBlock")
		code = self.wallet.newBlock(block, senderNetID=senderID)
		if (code):
			self.runAllTxs()

	def incomingBlockchain(self, blockchain):
		diagnostic ("Miner::incomingBlockchain")
		self.consensus(blockchain)

	def executeTransaction(self, recipientID, amount):
		"""
		Send <amount> NBCs to recipient, from this account
		"""
		diagnostic ("Miner::executeTransaction")
		inputs = self.getInputsForAmount(amount)
		if (inputs == None):
			print("ERROR - Insufficient funds for transaction!")
			return

		tx = Transaction(
				self.getInputsForAmount(amount),
				self.getPeerKey(recipientID),
				self.wallet.publicKey,
				amount
			)
		tx.signTransaction(self.wallet.privateKey)
		lenBefore = len(self.wallet.noobchain)
		self.wallet.noobchain.addBlock(tx)
		lenAfter = len(self.wallet.noobchain)
		self.finalizeTransaction(tx)
		self.broadcastTransaction(tx)
		if (lenBefore < lenAfter):
			self.broadcastBlock(self.wallet.noobchain.getLastLegitBlock())


	def getInputsForAmount(self, amount):
		diagnostic ("Miner::getInputsForAmount")
		me = self.getPeerById(self.id)
		return me.getFundsForAmount(amount)

	def finalizeTransaction(self, tx):
		diagnostic ("Miner::finalizeTransaction")
		donor = None if (not tx.fromAddress) else self.getPeerByKey(tx.fromAddress)
		recipient = self.getPeerByKey(tx.toAddress)
		diagnostic("BEFORE:")
		diagnostic(recipient.utxos)
		if (donor):
			diagnostic(donor.id)
			donor.updateUtxos(tx.inputs, (None if len(tx.outputs) == 1 else tx.outputs[0]))
			diagnostic(donor.utxos)
		recipient.updateUtxos(None, tx.outputs[-1])
		diagnostic("AFTER:")
		diagnostic(recipient.utxos)

	def getPeerByKey(self, key):
		diagnostic ("Miner::getPeerByKey")
		for p in self.peers:
			if (p.publicKey == key):
				return p
		print("ERROR - requested peer key doesn't seem to exist!")
		return None

	def getPeerById(self, pid):
		diagnostic ("Miner::getPeerById")
		for p in self.peers:
			if (p.id == pid):
				return p
		print("ERROR - requested peer id doesn't seem to exist: {}"
			.format(pid))
		return None

	def getPeerKey(self, networkID):
		"""
		Find the public key of someone in the peers list, given their ID.
		"""
		diagnostic ("Miner::getPeerKey")
		if (networkID == self.id):
			return self.wallet.publicKey
		for peer in self.peers:
			if (peer.id == networkID):
				return peer.publicKey
		# Should be unreachable
		diagnostic('Error - Network id {} invalid or unavailable'.format(networkID))
		return None

	def attemptBroadcastPeers(self):
		diagnostic ("Miner::attemptBroadcastPeers")
		if (len(self.peers) < NUM_PEERS):
			return -1
		peersJson = [p.serialize() for p in self.peers]
		for p in self.peers:
			if (p.id > 0):
				#RequestThread('post', p.url+'/peers', json={"peers": peersJson}).start()
				try:
				    requests.post(p.url+'/peers', json={"peers": peersJson}, timeout=0.0000000001)
				except requests.exceptions.ReadTimeout:
				    pass
		self.broadcastBlockchain()
		startuptxs = self.startupTransactionsExecution()
		self.massTxBroadcast(startuptxs)
		return len(self.peers)

	def massTxBroadcast(self, txs):
		diagnostic ("Miner::massTxBroadcast")
		for tx in txs:
			self.broadcastTransaction(tx)

	def broadcastTransaction(self, tx):
		diagnostic ("Miner::broadcastTransaction")
		for p in self.peers:
			if (p.id != self.id):
				jtx = tx.serialize()
				#RequestThread('post', p.url+'/transaction', json=jtx).start()
				try:
				    requests.post(p.url+'/transaction', json=jtx, timeout=0.0000000001)
				except requests.exceptions.ReadTimeout:
				    pass

	def broadcastBlock(self, block=None):
		diagnostic ("Miner::broadcastBlock")
		if (block is None):
			block = self.wallet.noobchain.getLastLegitBlock()
		for p in self.peers:
			if (p.id != self.id):
				#RequestThread('post', p.url+'/block', json={'block': block.serialize(), 'sid': self.id}).start()
				try:
				    requests.post(p.url+'/block', json={'block': block.serialize(), 'sid': self.id}, timeout=0.0000000001)
				except requests.exceptions.ReadTimeout:
				    pass


	def broadcastBlockchain(self):
		diagnostic ("Miner::broadcastBlockchain")
		for p in self.peers:
			if (p.id != self.id):
				#RequestThread('post', p.url+'/blockchain', json=self.wallet.noobchain.serialize()).start()
				try:
				    requests.post(p.url+'/blockchain', json=self.wallet.noobchain.serialize(), timeout=0.0000000001)
				except requests.exceptions.ReadTimeout:
				    pass

	def startupTransactionsExecution(self):
		diagnostic ("Miner::startupTransactionsExecution")
		if (not self.bootstrap):
			print("ERROR - Only bootstrap can distribute startup funds!")
			return
		self.wallet.peerList = self.peers
		me = self.getPeerById(0)
		ret = []
		for p in self.peers:
			if (p.id > 0):
				itx = Transaction(self.getInputsForAmount(START_BALANCE), p.publicKey, self.wallet.publicKey, START_BALANCE)
				itx.signTransaction(self.wallet.privateKey)
				diagnostic("GENERATED SIGNATURE:")
				diagnostic(itx.signature)
				diagnostic("GENERATED UID:")
				diagnostic(itx.uid)
				self.finalizeTransaction(itx)
				ret.append(itx)
		return ret

if __name__ == "__main__":
	diagnostic("Starting miner-client node...")
	builtins.guy = Miner()



# FLASK API ROUTES
