from Crypto.Hash import SHA256

from debug_utils import diagnostic

def key2String(someKey, delim=":"):
	if not someKey:	# genesis handling
		return ''
	return (str(someKey.publickey().n)+delim+str(someKey.publickey().e))

class Money:
    def __init__(self, transId, amount, recipient):
        diagnostic ("Money::init")
        self.transId = transId
        self.amount = amount
        self.recipient = recipient
        self.hash = SHA256.new(str(self).encode("utf8"))

    def __str__(self):
        diagnostic ("Money::str")
        return str(self.transId) + ', ' \
            + str(self.amount) + ', ' \
            + key2String(self.recipient)

    def __eq__(self, other):
        if (isinstance(other, Money)):
            return (self.transId == other.transId) and (self.amount == other.amount)
        else:
            raise Exception("Not instance of Money class")
        return False


    def serialize(self):
        diagnostic ("Money::serialize")
        if (not self.recipient):
            rec = "1"
        else:
            rec = key2String(self.recipient)
        return {
            "trans_id": self.transId,
            "amount": self.amount,
            "recipient_key": rec
        }
