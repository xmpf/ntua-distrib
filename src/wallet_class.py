#!/usr/bin/env python3

import binascii
import Crypto
import Crypto.Random
from Crypto.Hash import SHA256
from Crypto.PublicKey import RSA
from Crypto.Signature import PKCS1_v1_5 as PKCS
from transaction_class import Transaction
from blockchain_class import Blockchain
from block_class import Block
from money_class import Money
# Configuration Options
from miner_configuration import DIFFICULTY, NUM_PEERS, START_BALANCE
import requests
import utils

bits = 1024
class Wallet:
    def __init__(self):
        utils.diagnostic ("Wallet::init")
        self.randomGen = Crypto.Random.new().read
        self.privateKey = RSA.generate(bits, self.randomGen)
        self.publicKey = self.privateKey.publickey()
        self.signer = PKCS.new(self.privateKey)
        self.noobchain = Blockchain()
        self.cache = [] # list of tuples: (ordered_block_list, pid)

        self.id = -1
        self.peerList = []
        # Each wallet holds a copy of the blockchain

    # generateInitialBlock :: Wallet -> Blockchain ()
    def generateInitialBlock(self):
        """
        Generates one initial block containing the "Genesis" transaction
        """
        utils.diagnostic ("Wallet::generateInitialBlock")
        toAddress = self.publicKey.publickey()
        fromAddress = None     # FIND THIS!!!
        amount = NUM_PEERS * START_BALANCE
        genesisTransaction = Transaction(None, toAddress, fromAddress, amount, outputMoney=[Money(None, amount, toAddress)])
        genesisTransaction.signTransaction(self.privateKey)
        self.noobchain.append( Block("GENESIS", 1, True, True) )
        self.noobchain.addBlock(genesisTransaction)
        return genesisTransaction

    def newBlock(self, block, senderNetID=None):
        """
        New block, either local (unmined) or received (mined). Checks and then either
        appends or stores it for later.
        """
        utils.diagnostic ("Wallet::newBlock" )
        senderID = self.id
        if (senderNetID):
            senderID = senderNetID
        if (senderID == self.id):   # if block is local
            for i, entry in enumerate(self.cache): # check cache
                bks = entry[0]
                sid = entry[1]
                sema = {}
                for bk in bks:
                    sema.update(bk.txSet)
                if (block.txSet.issubset(sema)):    # if all our txs are given externally
                    self.restructureBlockChain(sid) # get everything
                    del self.cache[i]
                    return 'utxo-rerun'
            return self.noobchain.append(block, mined=False)    # otherwise ignore and add normally after mining
        else:   # if block is external store it for later
            for i in range(len(self.cache)):
                bks, sid = self.cache[i]
                if (sid == senderID):
                    self.cache[i][0].append(block)
                    return
            self.cache.append(([block], senderID))


    def restructureBlockChain(self, pid):
        """
        Get the entire blockchain from sid and replace ours if everything checks out.
        """
        utils.diagnostic ("Wallet::restructureBlockChain")
        peer = None
        for p in self.peerList:
            if (p.id == pid):
                peer = p
        if (not p):
            print("ERROR - requested peer id doesn't seem to exist: {}"
                .format(pid))
        return -1

        bcData = requests.get(peer.url+'/blockchain')
        fbc = cmd_cli.deserializeBlockChain(bcData) # LINTER ERROR cmd_cli ??
        if (fbc.checkValidity() and fbc.verifyBlockchain()):
            self.noobchain = fbc
            return 0
        return -1

    def __str__(self) -> str:
        utils.diagnostic ("Wallet::str")
        return "Public Key: " + str(self.publicKey) + "\n" \
             + "Blockchain: " + str(self.noobchain) + "\n" \
             + "Network ID: " + str(self.id)

    # @property
    # def pubKey(self):
    #     print ("Wallet::pubKey")
    #     # alias for self.pubAddress
    #     return self.pubAddress
    #
    # @property
    # def pvtKey(self):
    #     print ("Wallet::pvtKey")
    #     # alias for self.pvtAddress
    #     return self.pvtAddress
    #
    # @property
    # def pubAddress(self):
    #     print ("Wallet::pubAddress")
    #     if self == None:
    #         raise Exception("No wallet available")
    #     else:
    #         return binascii.hexlify(self.publicKey.exportKey(format="DER")).decode("ascii")
    #
    # @property
    # def pvtAddress(self):
    #     print ("Wallet::pvtAddress")
    #     if self == None:
    #         raise Exception("No wallet available")
    #     else:
    #         return binascii.hexlify(self.privateKey.exportKey(format="DER")).decode("ascii")
    #
    # @property
    # def keys(self):
    #     print ("Wallet::keys")
    #     contents = {
    #         "privateKey": self.pvtAddress
    #       , "publicKey": self.pubAddress
    #     }
    #     return contents
