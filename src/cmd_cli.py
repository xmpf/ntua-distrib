#!/usr/bin/env python3

# FLASK IMPORTS
from flask import Flask, jsonify, request, abort
from flask_api import status
from transaction_class import Transaction, key2String, string2Key
import threading
import time
import signal
import logging
from utils import *

from miner_configuration import *

import binascii
# import builtins
import sys
import cmd
import time
# custom file api
# from nbc_api import *
import os.path

import re

from blockchain_class import Blockchain
from block_class import Block
from miner import Miner
from miner_configuration import *
from peer_class import Peer

from money_class import Money

from debug_utils import *

newTr = 't'
fromFileSwitches = ['--from_file', '-f']
totalBalanceSwitches = ['--all', '-a']
view = 'view'
balance = 'balance'
bootstrap = False
nbcterm = None

class NBCTerminal(cmd.Cmd):
    prompt = '+> '
    intro = "Miner-client terminal interface."
    doc_header = 'doc_header'
    misc_header = 'misc_header'
    undoc_header = 'undoc_header'
    ruler = '-'

    def __init__(self):
        """
        Startup the command listener and initialize the mining node
        """
        diagnostic("NBCTerminal::init")
        cmd.Cmd.__init__(self)
        self.guy=None
        self.glock = threading.Lock()
        print("CLI initialized!")

    def precmd(self, s: str) -> str:
        """
        Sanitize command string from the terminal and prepare for execution
        """
        return re.sub(r'\s+', ' ', s.lower().rstrip())

    def do_balance(self, arg):
        """
        Display current account balance, based on local blockchain history
        """
        if (arg in totalBalanceSwitches):
            self.guy.printGlobalWalletState()
        else:
            self.guy.printWalletBalance()


    def do_view(self, arg):
        """
        Callback for view command
        """
        print(self.guy.viewLastBlock())

    def do_t(self, arg):
        """
        Callback for transaction command
        """
        arglist = arg.split(' ')
        diagnostic(arglist)
        if (arglist[0] in fromFileSwitches):
            with open(arglist[1], 'r') as f:
                recvs = []
                amnts = []
                batch = [line.rstrip().split('\t') for line in f if len(line) > 1]
                for trans in batch:
                    assert(len(trans) == 2)
                    recvs.append(int(trans[0]))
                    amnts.append(float(trans[1]))
                with self.glock:
                    for i in range(len(amnts)):
                        if (amnts[i] <= 0):
                            continue
                        self.guy.executeTransaction(recvs[i], amnts[i])
                        time.sleep(3)
        else:
            _id = int(arglist[0])
            if len(self.guy.peers) >= _id:
                recipient = _id
            else:
                print("Error: Id does not exists")
                return 0
            amount = float(arglist[1])
            if (amount > 0):
                with self.glock:
                    self.guy.executeTransaction(recipient, amount)

    def do_exit(self, arg):
        print("Exiting command line")
        return True

def cmdline():
    global nbcterm
    nbcterm.guy = Miner(bootstrap)
    if (not nbcterm.guy.bootstrap):
        nbcterm.guy.networkCommBootstrap()
    nbcterm.cmdloop()

def api_thread():
    global nbcterm
    app.run(MINER_ADDRESS, MINER_PORT)

class thread (threading.Thread):
    def __init__(self, func, threadID, name, sleep):
      threading.Thread.__init__(self)
      self.threadID = threadID
      self.name = name
      self.sleep = sleep
      self.func = func
    def run(self):
        time.sleep(self.sleep)
        self.func()

# FLASK CODE
log = logging.getLogger('werkzeug')
log.setLevel(logging.CRITICAL)

app_name = 'nbc'
app = Flask(app_name)

@app.errorhandler(400)
def bad_request(error):
    return 'Bad request', 400
@app.errorhandler(404)
def resource_not_found(error):
    return 'Not Found', 404
@app.errorhandler(403)
def resource_not_found(error):
    return 'Forbidden', 403


# This API endpoint is called on startup, GET requests coming in from clients
# to the bootstrap so that client nodes get proper ids.
@app.route('/idHandout', methods=["POST"])
def idHandout():
    global nbcterm
    guy = nbcterm.guy
    if (guy.bootstrap):
        data = request.get_json(force=True)
        newPeerID = guy.getNextId(data["publicKey"], data["full_url"])
        return jsonify({"id": newPeerID, "publicKey": key2String(guy.wallet.publicKey)}), 200
    abort(400)

@app.route('/broadcastCheck', methods=["GET"])
def broadcastCheck():
    global nbcterm
    guy = nbcterm.guy
    if (not guy.bootstrap):
        abort(403)
    res = guy.attemptBroadcastPeers()
    if (res < 0):
        abort(404)
    else:
        return 'OK', 200

@app.route('/peers', methods=["POST"])
def peers():
    global nbcterm
    guy = nbcterm.guy
    if (not guy.bootstrap):
        data = request.get_json(force=True)
        guy.expandPeers(data)
        guy.wallet.peerList = guy.peers
        return 'OK', 200
    abort(403)

@app.route('/transaction', methods=["POST"])
def transaction():
    global nbcterm
    guy = nbcterm.guy
    glock = nbcterm.glock
    with glock:
        if not request.get_json(force=True):
            abort(400)
        data = request.get_json(force=True)
        tx = deserializeTx(data)
        guy.incomingTransaction(tx)
        return "OK", 200

@app.route('/block', methods=["POST", "GET"])
def block():
    global nbcterm
    guy = nbcterm.guy
    glock = nbcterm.glock
    with glock:
        if request.method == "GET":
            data = guy.wallet.noobchain.lastBlock().serialize()
            return jsonify(data), 200
        elif request.method == "POST":
            if not request.get_json(force=True):
                abort(400)
            data = request.get_json(force=True)
            sid = int(data['sid'])
            incBlock = deserializeBlock(data['block'])
            incBlock.txSet.update(incBlock.transactions)
            guy.incomingBlock(incBlock, sid)
            return "OK", 200

@app.route('/blockchain', methods=["POST", "GET"])
def blockchain():
    global nbcterm
    guy = nbcterm.guy
    glock = nbcterm.glock
    diagnostic('REACHED LOCK BARRIER')
    with glock:
        diagnostic('CROSSED LOCK BARRIER')
        if request.method == "GET":
            data = guy.wallet.noobchain.serialize()
            return jsonify(data), 200
        elif request.method == "POST":
            diagnostic('POSTING')
            if not request.get_json(force=True):
                abort(400)
            data = request.get_json(force=True)
            diagnostic('DESERIALIZING')
            guy.incomingBlockchain(deserializeBlockChain(data))
            diagnostic('DONE')
            return "OK", 200


if __name__ == '__main__':

    if (len(sys.argv) == 2 and sys.argv[1] == 'bootstrap'):
        print("Starting in bootstrap mode...")
        bootstrap = True
    else:
        bootstrap = False
        print("Starting in client mode...")

    api_thread = thread(api_thread, 1, "API thread", 0)
    cmd_thread = thread(cmdline, 2, "Command Line thread", 1)

    api_thread.setDaemon(True)
    #cmd_thread.setDaemon(True)
    api_thread.start()
    nbcterm = NBCTerminal()
    cmd_thread.start()

    def sigint(sig, frame):
        print("\nExiting program")
        # we have to do stuff to gracefully exit the cmdline thread
        # because if not it captures the stdout and we don't have console output
        # to fix it if pressing just Ctrl-C type stty echo in terminal
        # For now the best way to close it is pressing exit and then Ctrl-C
        sys.exit(1)

    signal.signal(signal.SIGINT, sigint)
