from miner_configuration import DEBUG
import time

def diagnostic(msg):
    if (DEBUG):
        print(msg)

def debugSleep(s):
    if (DEBUG):
        time.sleep(s)
