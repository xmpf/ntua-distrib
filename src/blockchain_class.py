#!/usr/bin/env python3

import binascii
import time

# Block Class
from block_class import Block

# Transaction Class
from transaction_class import Transaction

# Configuration Options
from miner_configuration import DIFFICULTY, NUM_PEERS, START_BALANCE

from debug_utils import diagnostic

# Blockchain Class
class Blockchain:
    def __init__(self):
        diagnostic ("Blockchain::init")
        self.difficulty = DIFFICULTY      # number of leading zeros
        self.blockchain = []              # list of blocks
        self.hashSet = set()

    def append(self, block: Block, mined=True):
        diagnostic ("Blockchain::append")

        if (len(self.blockchain) == 0):
            self.blockchain.append(block)
            return -1

        if (not mined):
            self.mineBlock(block)

        lastCheckableBlock = self.getLastLegitBlock()
        if (lastCheckableBlock):
            if (lastCheckableBlock.hash != block.previousHash):
                if (block.previousHash in self.hashSet):
                    for i, b in enumerate(self.blockchain):
                        if (b.hash == block.previousHash):
                            return i
                    return -2
                diagnostic("BLOCK APPEND - HASH CHAIN FAILED")
                diagnostic(lastCheckableBlock.hash)
                diagnostic(block.previousHash)
                return -2

        self.blockchain.append(block)
        self.hashSet.add(block.hash)
        return -1


    def __str__(self) -> str:
        diagnostic ("Blockchain::str")
        ret = ""
        for i in range (0, len(self.blockchain)):
            ret += str(self.blockchain[i])
        return ret

    # len :: Blockchain -> Integer
    def __len__(self) -> int:
        """
        Returns length of blockchain if available, zero otherwise
        """
        diagnostic ("Blockchain::len")
        if (len(self.blockchain) == 0):
            return 0
        else:
            return len(self.blockchain) - (1 if (not self.blockchain[-1].valid) else 0)

    # checkValidity :: Blockchain -> Boolean
    def checkValidity(self) -> bool:
        """
        Checks that every block has a previous hash field equal to the hash
        of the previous block in the blockchain
        """
        diagnostic ("Blockchain::checkValidity")
        prev_block = self.blockchain[0]
        for block in self.blockchain[1:]:
            if (block.previousHash != prev_block.computeHash()):
                return False
            prev_block = block
        return True

    # checkNonce :: Blockchain -> Block -> Boolean
    def checkNonce(self, block: Block) -> bool:
        """
        Computes block hash and checks if it matches the difficulty parameter
        """
        diagnostic ("Blockchain::checkNonce")
        baseProof = block.computeHash()
        proof = int(baseProof, 16)     # computed hash
        lim = 1 << (4*len(block.hash) - DIFFICULTY)
        return proof < lim


    # mineBlock :: Block -> ()
    def mineBlock(self, block: Block):
        """
        Mines a block by finding a nonce that satisfies the difficulty param
        """
        diagnostic ("Blockchain::mineBlock")
        if (block == None):
            block = self.lastBlock()      # get last block
        if(block.isMinable()):  # if the block is minable
            counter = 0
            while(not self.checkNonce(block)):  # check a new nonce
                counter += 1    # until it works
            block.validateBlock()   # then validate
            diagnostic('BLOCK MINED - STEPS TAKEN: {:}'.format(counter))

    # addBlock :: Blockchain -> Transaction -> Blockchain
    def addBlock(self, tx: Transaction):
        """
        Add a transaction to the blockchain
        """
        diagnostic ("Blockchain::addBlock")
        if (self.lastBlock().addTransaction(tx)): # if the last block has room
            return True # add the transaction there
        else:
            self.mineBlock(self.lastBlock())    # else mine the last block
            self.blockchain.append( # add a new empty one
                Block(None, self.lastBlock().hash)
            )
            self.addBlock(tx)   # and add the tx there

    def verifyBlockchain(self) -> bool:
        """
        Used to verify if all blocks in the chain contain verified txs
        """
        diagnostic ("Blockchain::verifyBlockchain")
        for b in self.blockchain:
            if (not b.verifyBlockTxs()):
                return False
        return True

    # getCurrentBalance :: Blockchain -> PublicKey -> Float
    # def getCurrentBalance(self, accountKey: str) -> float:
    #     """
    #     Get current balance, assuming zero to start, based on the complete
    #     blockchain history.
    #     """
    #     print ("Blockchain::getCurrentBalance")
    #     ret = 0.0
    #     for b in self.blockchain:
    #         ret += b.getTotalChange(accountKey)
    #     return ret

    def getLastLegitBlock(self):
        """
        Returns block instance
        """
        diagnostic ("Blockchain::getLastLegitBlock")
        if self.blockchain == None:
            raise Exception("Blockchain not initialized")
        if (self.blockchain[-1].isValid()):
            return self.blockchain[-1]
        if (len(self.blockchain) == 1):
            print('No blocks validated yet!')
            return None
        return self.blockchain[-2]

    def getLastValidatedBlock(self):
        """
        Frontend spec, display last validated block
        """
        diagnostic ("Blockchain::getLastValidatedBlock")
        if self.blockchain == None:
            raise Exception("Blockchain not initialized")
        if (self.blockchain[-1].isValid()):
            return str(self.blockchain[-1])
        if (len(self.blockchain) == 1):
            print('No blocks validated yet!')
            return None
        return str(self.blockchain[-2])

    def serialize(self):
        diagnostic ("Blockchain::serialize")
        length = len(self.blockchain)
        blockchain = [x.serialize() for x in self.blockchain] if length > 0 else []
        return {
            "blockchain": blockchain,
            "length": length
        }

    # lastBlock :: Blockchain -> Block
    def lastBlock(self) -> Block:
        """
        Instead of writing self.blockchain[-1] every single time
        added lastBlock as property of Blockchain Class.
        Returns last block in list.
        """
        diagnostic ("Blockchain::lastBlock")
        if (not self.blockchain):
            raise Exception("Blockchain is empty")
        elif len(self.blockchain) > 0:
            return self.blockchain[-1]


"""
# main program
if __name__ == '__main__':
    bc = Blockchain()
    N = 100
    txs = []
    for i in range(N):
        txs.append(Transaction(i, i+1, 100))
    for tx in txs:
        bc.addBlock(tx)
    print(len(bc))

    for block in (bc.blockchain):
        print(str(block))

    # This prints an error message, without crashing though.
    bc.addBlock(txs[-1])

"""
