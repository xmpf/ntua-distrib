from Crypto.PublicKey import RSA
import binascii
import threading
import requests

from money_class import Money
from transaction_class import *
from block_class import Block
from blockchain_class import Blockchain

def deserializeMoney(utxo):
    recipientKey = string2Key(utxo["recipient_key"])
    return Money(
            utxo["trans_id"],
            float(utxo["amount"]),
            recipientKey
        )

def deserializeTx(data):
    inputMoney = []
    outputMoney = []
    if (data["inputs"]):
        for utxo in data["inputs"]:
            print("DES_INPUTS: " + str(utxo['amount']))
            inputMoney.append(deserializeMoney(utxo))
    for utxo in data["outputs"]:
        print("DES_OUTPUTS: " + str(utxo['amount']))
        outputMoney.append(deserializeMoney(utxo))

    toKey = string2Key(data["to"])
    if (not isinstance(data['from'], int)):
        fromKey = string2Key(data["from"])
    else:
        fromKey = None
    ret = Transaction(
        inputMoney,
        toKey,
        fromKey,
        float(data["amount"]),
        outputMoney=outputMoney
    )
    sig = data['signature']
    ret.signature = (binascii.unhexlify(sig) if (sig) else None)
    return ret

def deserializeBlock(data):
    incBlock = Block(data['data'], data['previousHash'])
    incBlock.nonce = int(data['nonce'])
    incBlock.hash = data['hash']
    incBlock.transactions = [deserializeTx(txd) for txd in data['transactions']]
    print('RECEIVED {:} INPUT TRANSACIONS'.format(len(data['transactions'])))
    [print(str(deserializeTx(txd))) for txd in data['transactions']]
    incBlock.valid = True
    return incBlock

def deserializeBlockChain(data):
    ret = Blockchain()
    for b in data['blockchain']:
        ret.append(deserializeBlock(b))
    return ret


# class RequestThread(threading.Thread):
#     def __init__(self, type, url, json=None):
#         super(RequestThread, self).__init__()
#         print('REQUEST THREAD STARTING... '+url)
#         self.type = type
#         self.url = url
#         if (json):
#             self.data = json
#
#     def run(self):
#         print('REQUEST THREAD RUNNING... '+ self.url)
#         if (self.type == 'post'):
#             requests.post(self.url, self.data)
#         elif (self.type == 'get'):
#             requests.get(self.url)
#         else:
#             print('UNKNOWN REQUEST TYPE: '+self.type)
#         print('REQUEST THREAD COMPLETE: '+self.url)
