#!/usr/bin/env python3

import sys
import click
import pickle

import os.path

from miner import Miner
from miner_configuration import MINING_SESS_FNAME

prompt = '>'
newTr = 't'
view = 'view'
balance = 'balance'

@click.group()
def main():
    pass

@main.command(help='Start client-miner node on this machine.')
@click.option('--bootstrap', '-b', is_flag=True,
    help='Set true if execution node is the bootstrap node of the network.')
def startup(bootstrap):
    # leader = bootstrap
    if(bootstrap):
        click.echo('Starting in bootstrap mode')
    else:
        click.echo('Starting in client mode')
    guy = Miner(bootstrap)
    pickle.dump(guy, open(MINING_SESS_FNAME, 'wb'))


@main.command(newTr, help='Execute a transaction, interactively or in file mode.')
@click.option('--from_file', '-f', type=click.File('r'),
    help='Give the relative path to an execution file with format: <recipient_net_id>\\t<amount>\\n')
@click.argument('recipient', default="")
@click.argument('amount', default=0.0)
def transact(recipient, amount, from_file=None):
    if (not os.path.isfile(MINING_SESS_FNAME)):
        click.echo('No mining session initialized - run startup first!')
    if (from_file != None):
        recvs = []
        amnts = []
        batch = [line.rstrip().split('\t') for line in from_file if len(line) > 1]
        for trans in batch:
            assert(len(trans) == 2)
            recvs.append(trans[0])
            amnts.append(float(trans[1]))

        click.echo(batch)
        for (rec, am) in batch:
            executeTransaction(rec, am)
    else:
        click.echo(recipient)
        click.echo(amount)
        executeTransaction(recipient, amount)

def executeTransaction(recipient, amount):
    assert(os.path.isfile(MINING_SESS_FNAME))
    guy = pickle.load(open(MINING_SESS_FNAME, 'rb'))
    guy.executeTransaction(recipient, amount)


@main.command(view, help='View last validated block in this miner\'s chain.')
def view():
    assert(os.path.isfile(MINING_SESS_FNAME))
    guy = pickle.load(open(MINING_SESS_FNAME, 'rb'))
    click.echo(guy.viewLastBlock())

@main.command(balance, help='View current estimated balance based on client\'s blockchain history')
def balance():
    assert(os.path.isfile(MINING_SESS_FNAME))
    guy = pickle.load(open(MINING_SESS_FNAME, 'rb'))
    click.echo(guy.getWalletBalance())

if __name__=='__main__':
    main()
