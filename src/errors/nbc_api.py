from flask import Flask, jsonify,request, abort
from flask_api import status
from transaction_class import Transaction, key2String, string2Key
import threading
import time
import signal
import logging

from miner_configuration import *

from money_class import Money

log = logging.getLogger('werkzeug')
log.setLevel(logging.CRITICAL)
#from builtins import guy as guy
#from bultins import lock as lock
import builtins

def startServer(port):
    app_name = 'nbc'
    app = Flask(app_name)
    app = Flask(__name__.split('.')[0])

    @app.errorhandler(400)
    def bad_request(error):
        return 'Bad request', 400
    @app.errorhandler(404)
    def resource_not_found(error):
        return 'Not Found', 404
    @app.errorhandler(403)
    def resource_not_found(error):
        return 'Forbidden', 403


    # This API endpoint is called on startup, GET requests coming in from clients
    # to the bootstrap so that
    @app.route('/idHandout', methods=["POST"])
    def idHandout():
        if (guy.bootstrap):
            data = request.get_json(force=True)
            newPeerID = guy.getNextId(data["publicKey"], data["full_url"])
            return jsonify({"yourID": newPeerID, "myPublicKey": key2String(guy.wallet.publicKey)}), 200
        abort(400)

    @app.route('/broadcastCheck', methods=["GET"])
    def broadcastCheck():
        if (not guy.bootstrap):
            abort(403)
        res = guy.attemptBroadcastPeers()
        if (res < 0):
            abort(404)
        else:
            return 'OK', 200

    @app.route('/peers', methods=["POST"])
    def peers():
        if (not guy.bootstrap):
            data = request.get_json(force=True)
            guy.expandPeers(data)
            return 'OK', 200
        abort(403)

    @app.route('/transaction', methods=["POST"])
    def transaction():
        if not request.get_json(force=True):
            abort(400)
        data = request.get_json(force=True)
        inputMoney = []
        outputMoney = []
        for utxo in data["inputs"]:
            recipientKey = string2Key(utxo["recipient_key"])
            inputMoney.append(
                Money(
                    utxo["trans_id"],
                    float(utxo["amount"]),
                    recipientKey
                )
            )
        for utxo in data["outputs"]:
            recipientKey = string2Key(utxo["recipient_key"])
            ouputMoney.append(
                Money(
                    utxo["trans_id"],
                    float(utxo["amount"]),
                    recipientKey
                )
            )

        toKey = string2Key(data["to"])
        fromKey = string2Key(data["from"])
        guy.executeTransaction(
            Transaction(
                inputMoney,
                toKey,
                fromKey,
                float(data["amount"]),
                outputMoney=outputMoney
            )
        )
        return "OK", 200

    @app.route('/block', methods=["POST", "GET"])
    def block():
        if request.method == "GET":
            data = {
                    "name" : "block",
                    "id": 2
                    }
            return jsonify(data), 200
        elif request.method == "POST":
            if not request.get_json(force=True):
                abort(400)
            data = request.get_json(force=True)
            return "OK", 200

    @app.route('/blockchain', methods=["POST", "GET"])
    def blockchain():
        if request.method == "GET":
            data = {
                    "name" : "chain",
                    "id": 3
                    }
            return jsonify(data), 200
        elif request.method == "POST":
            if not request.get_json(force=True):
                abort(400)
            data = request.get_json(force=True)
            return "OK", 200

    @app.route('/connect', methods=["POST"])
    def connect():
        if not request.get_json(force=True):
            abort(400)
        # we suppose that the node sends their publik key
        # as { "public_key" : "somthing" }
        data = request.get_json(force=True)
        pbk = data["public_key"]
        # the bootstrap node has to return the id as a reply

    @app.route('/nodes', methods=["POST"])
    def nodes():
        if not request.get_json(force=True):
            abort(400)
        """
        we suppose that the json object is a list of objects
        # [
            {
                "ip_a ddress" : ,
                "port" : ,
                "public_key":
            },
            ...
        ]
        """

    app.run(MINER_ADDRESS, port)
