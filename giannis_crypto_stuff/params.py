#!/usr/bin/env python3
# coding: utf-8

RSA_KEY_BITS = 1024
MSG_LEN_BYTES = RSA_KEY_BITS // 8
ENDIAN = 'big'
MESSAGE = b"I love deadlines. I love the whooshing noise they make as they go by."
