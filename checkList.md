## Transaction
- [X] Transaction::serialize
- [ ] Transaction::verifyTransaction
- [ ] Transaction::getChange

## Block
- [X] Constant Block Size (Capacity)
- [X] Block::serialize
- [ ] Block::addTransaction

## Wallet
- [X] Wallet::init
- [ ] Wallet::generateInitialBlock
- [ ] Wallet Keys and Addresses

## Blockchain
- [X] Blockchain::serialize
- [X] Blockchain::append
- [ ] Blockchain::len

## Miner
- [ ] Reward proportional to the number of connected peers

## Peer
- [ ] Registration -> peerID, peerList, ...

## API
- [ ] /connect <bootstrap> endpoint -> register self to bootstrap
- [ ] /blockchain endpoint -> returns whole blockchain as JSON
