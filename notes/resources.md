## Simple Blockchain in Javascript
[Creating a blockchain with Javascript (Blockchain, part 1)](https://www.youtube.com/watch?v=zVqczFZr124)
[Implementing Proof-of-Work in Javascript (Blockchain, part 2)](https://www.youtube.com/watch?v=HneatE69814)
[Miner rewards & transactions - Blockchain in Javascript (part 3)](https://www.youtube.com/watch?v=fRV6cGXVQ4I)
[Signing transactions - Blockchain in Javascript (part 4)](https://www.youtube.com/watch?v=kWQ84S13-hw)

## HyperLedger
[Building BLOCKCHAIN Apps With HYPERLEDGER COMPOSER](https://www.youtube.com/watch?v=gAxK6zYrfxI)

## cppcon
[Developing Blockchain Software](https://www.youtube.com/watch?v=RRP65VvIgGg)

## Github Projects
[Python Blockchain #1](https://github.com/satwikkansal/python_blockchain_app/blob/master/step_by_step_tutorial.md)

