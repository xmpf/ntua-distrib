## Genesis Block
- Block#0 (hardcoded)  

## Mining / Miners
- Transaction fees  
- Miner authenticates each transaction using sender\'s public key
- Miner checks if transaction is valid (enough balance, ...)

## Client
- create_account  
- send_coins
- 

## Wallet
- Private/Public Key generation for addresses (RSA, ECC)  
```
- First, generate the keccak-256 hash of the public key. It will give you a 256-bit
number.
- Drop the first 96 bits, that is, 12 bytes. You should now have 160 bits of binary
data, that is, 20 bytes.
- Now encode the address as a hexadecimal string. So finally, you will have a
bytestring of 40 characters, which is your account address.
```

## Communication
- Implement a basic communication for testing purposes (requirement: same genesis block)  
- Bootstrap node (first node)  

## Consensus
- Proof of work (nonce, constant mining difficulty)  
- When a transaction is valid (signed, enough balance, ...)  
- When a block is valid (constant number of transaction in a block, timestamp validation)  
- Longest chain wins (no confirmations for transactions)  

## Security Attacks
- Race attack: replay attack  
- Finney attack:  
- Majority attack:  
